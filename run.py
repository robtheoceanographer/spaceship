#!/usr/bin/env python
"""

an example of how to use the spaceship package.

"""
# Prototype the southern ocean chl plotting stuff.
import matplotlib
matplotlib.use('Agg')
import spaceship
import os
import arrow
chl4investigator = spaceship.Sat2Ship()
chl4investigator.customer_email = 'nic.pittman@investigator.csiro.au'
# chl4investigator.customer_email = 'robert.johnson@utas.edu.au'
chl4investigator.northernmost_lat = '-40.0'
chl4investigator.southernmost_lat = '-70.0'
chl4investigator.easternmost_lon = '170.0'
chl4investigator.westernmost_lon = '140.0'
# Make chl images:
arw = arrow.utcnow()
arw = arw.shift(days=-2)
date_of_interest = arw.format('YYYYMMDD')
print(date_of_interest)
# date_of_interest = '20181014'
chl4investigator.get_a_sat_file(date_of_interest, 'CHL', 'DAILY', 'JOHNSON')
chl4investigator.chl_file_suffix = 'southern_ocean_chl.nc'
chl4investigator.plot_max = 2
chl4investigator.make_chl_plot(str('MODISA CHL - Johnson - ' + date_of_interest))
os.rename('/tmp/spaceship/tmp_southern_ocean_chl.png', str('/tmp/spaceship/modisAquaSouthernOceanChlJohnsonAlgorithm' + date_of_interest + '.png'))
chl4investigator.copy_to_dir('/home/rjohnson/imstore/')
chl4investigator.email_the_png()
chl4investigator.cleanup()


# SST - 1 Day.
sst4investigator = spaceship.Sat2Ship()
sst4investigator.customer_email = 'nic.pittman@investigator.csiro.au'
# sst4investigator.customer_email = 'robert.johnson@utas.edu.au'
sst4investigator.northernmost_lat = '-40.0'
sst4investigator.southernmost_lat = '-70.0'
sst4investigator.easternmost_lon = '170.0'
sst4investigator.westernmost_lon = '140.0'
# Make chl images:
arw = arrow.utcnow()
arw = arw.shift(days=-2)
date_of_interest = arw.format('YYYYMMDD')
print(date_of_interest)
# sst4investigator.sst_tds_url = 'http://thredds.aodn.org.au/thredds/catalog/IMOS/SRS/SST/ghrsst/L3S-3d/ngt/2018/catalog.html'
# sst4investigator.sst_file_suffix = 'AVHRR_D-3d_night.nc'
sst4investigator.get_a_sat_file(date_of_interest, 'SST')
sst4investigator.plot_max = None
sst4investigator.make_sst_plot()
sst4investigator.copy_to_dir('/home/rjohnson/imstore/')
sst4investigator.email_the_png()
sst4investigator.cleanup()



# import spaceship
# chl4investigator = spaceship.Sat2Ship()
# chl4investigator.customer_email = 'nic.pittman@utas.edu.au'
# chl4investigator.customer_email = 'robert.johnson@utas.edu.au'
# chl4investigator.northernmost_lat = '-30.0'
# chl4investigator.southernmost_lat = '-50.0'
# chl4investigator.easternmost_lon = '160.0'
# chl4investigator.westernmost_lon = '135.0'

# # Make chl images:
# chl4investigator.plot_max = 1
# chl4investigator.get_a_sat_file('20180923', 'CHL')
# chl4investigator.make_chl_plot()
# chl4investigator.copy_to_dir('/Users/rjohnson/')
# chl4investigator.email_the_png()
# chl4investigator.cleanup()

# # Make SST images:
# chl4investigator.get_a_sat_file('20181001', 'SST')
# chl4investigator.plot_max = None
# chl4investigator.make_sst_plot()
# chl4investigator.copy_to_dir('/Users/rjohnson/')
# chl4investigator.email_the_png()
# chl4investigator.cleanup()
#
