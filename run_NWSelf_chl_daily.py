#!/usr/bin/env python
"""

an example of how to use the spaceship package.

"""
# Prototype the southern ocean chl plotting stuff.
import matplotlib
matplotlib.use('Agg')
import spaceship
import os
import arrow
import netCDF4
import numpy
import glob
import os
import datetime

chl4investigator = spaceship.Sat2Ship()
# chl4investigator.customer_email = 'nic.pittman@investigator.csiro.au'
chl4investigator.customer_email = 'robert.johnson@utas.edu.au'
chl4investigator.northernmost_lat = '-6.0'
chl4investigator.southernmost_lat = '-22.0'
chl4investigator.easternmost_lon = '122.0'
chl4investigator.westernmost_lon = '102.0'


for dd in [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]:
    arw = arrow.get(datetime.datetime(2018, 11, dd))
    start_date = arw.format('YYYYMMDD')
    print(start_date)
    chl4investigator.get_a_sat_file(start_date, 'CHL', 'DAILY', 'OCX')
    chl4investigator.chl_file_suffix = '.L3m_DAY_SNPP_CHL_chl_ocx_4km.nc'
    chl4investigator.plot_max = 1
    chl4investigator.make_chl_plot(str('VIIRS CHL, Daily, for: ' + start_date))
    chl4investigator.copy_to_dir('/home/rjohnson/2.CURRENT_PROJECTS/spaceship')
    chl4investigator.cleanup()
