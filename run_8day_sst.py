#!/usr/bin/env python
"""

an example of how to use the spaceship package.

"""
# SST 8 day average.
import matplotlib
matplotlib.use('Agg')
import spaceship
import os
import arrow
import netCDF4
import numpy
import glob
import os

sst4investigator = spaceship.Sat2Ship()
sst4investigator.customer_email = 'nic.pittman@investigator.csiro.au'
#sst4investigator.customer_email = 'robert.johnson@utas.edu.au'
sst4investigator.northernmost_lat = '-40.0'
sst4investigator.southernmost_lat = '-70.0'
sst4investigator.easternmost_lon = '170.0'
sst4investigator.westernmost_lon = '140.0'
arw = arrow.utcnow()
start_date = arw.shift(days=-10).format('YYYYMMDD')
print(start_date)
end_date = arw.shift(days=-3).format('YYYYMMDD')
print(end_date)
sst4investigator.cleanup()
sst4investigator.get_a_sat_file(arw.shift(days=-3).format('YYYYMMDD'), 'SST')
sst4investigator.get_a_sat_file(arw.shift(days=-4).format('YYYYMMDD'), 'SST')
sst4investigator.get_a_sat_file(arw.shift(days=-5).format('YYYYMMDD'), 'SST')
sst4investigator.get_a_sat_file(arw.shift(days=-6).format('YYYYMMDD'), 'SST')
sst4investigator.get_a_sat_file(arw.shift(days=-7).format('YYYYMMDD'), 'SST')
sst4investigator.get_a_sat_file(arw.shift(days=-8).format('YYYYMMDD'), 'SST')
sst4investigator.get_a_sat_file(arw.shift(days=-9).format('YYYYMMDD'), 'SST')
sst4investigator.get_a_sat_file(arw.shift(days=-10).format('YYYYMMDD'), 'SST')

# load all the sst data and do an nanmean
filenames = glob.glob(str(sst4investigator.tmp_working_dir + '/*' + sst4investigator.sst_file_suffix))

# must have lat and lon too...
nc = netCDF4.Dataset(filenames[0])
nc.set_auto_maskandscale(True)
lon_subset = nc['lon'][:][numpy.abs(nc['lon'][:] - float(sst4investigator.westernmost_lon)).argmin():numpy.abs(nc['lon'][:] - float(sst4investigator.easternmost_lon)).argmin()]
lat_subset = nc['lat'][:][numpy.abs(nc['lat'][:] - float(sst4investigator.northernmost_lat)).argmin():numpy.abs(nc['lat'][:] - float(sst4investigator.southernmost_lat)).argmin()]
nc.close()

sst_sandwich = numpy.empty([8,lat_subset.shape[0], lon_subset.shape[0]])

def sst_extract(sst4investigator, filename):
    nc = netCDF4.Dataset(filename)
    nc.set_auto_maskandscale(True)
    attributes = [str(a) for a in nc['sea_surface_temperature'].ncattrs()]
    # Get the fill value
    if '_FillValue' in attributes:
        fill_value = nc['sea_surface_temperature']._FillValue
    else:
        fill_value = False
    lon_subset = nc['lon'][:][numpy.abs(nc['lon'][:] - float(sst4investigator.westernmost_lon)).argmin():numpy.abs(nc['lon'][:] - float(sst4investigator.easternmost_lon)).argmin()]
    lat_subset = nc['lat'][:][numpy.abs(nc['lat'][:] - float(sst4investigator.northernmost_lat)).argmin():numpy.abs(nc['lat'][:] - float(sst4investigator.southernmost_lat)).argmin()]
    print('Masking fill values')
    if fill_value is not False:
        fill_mask = nc['sea_surface_temperature'][0,:,:].data == fill_value
    sst_bias_corrected = nc['sea_surface_temperature'][0,:,:] - nc['sses_bias'][0,:,:]
    print('Re-applying fill values')
    if fill_value is not False:
        sst_bias_corrected[fill_mask] = fill_value
    print('Turning fill values into nans')
    sst_bias_corrected[numpy.where(sst_bias_corrected == fill_value)] = numpy.nan
    #sst_bias_corrected.fill_value = numpy.nan
    sst_bias_corrected_subset = sst_bias_corrected[numpy.abs(nc['lat'][:] - float(sst4investigator.northernmost_lat)).argmin():numpy.abs(nc['lat'][:] - float(sst4investigator.southernmost_lat)).argmin(), numpy.abs(nc['lon'][:] - float(sst4investigator.westernmost_lon)).argmin():numpy.abs(nc['lon'][:] - float(sst4investigator.easternmost_lon)).argmin()]
    sst_quality_levels_subset = nc['quality_level'][0,:,:][numpy.abs(nc['lat'][:] - float(sst4investigator.northernmost_lat)).argmin():numpy.abs(nc['lat'][:] - float(sst4investigator.southernmost_lat)).argmin(), numpy.abs(nc['lon'][:] - float(sst4investigator.westernmost_lon)).argmin():numpy.abs(nc['lon'][:] - float(sst4investigator.easternmost_lon)).argmin()]
    nc.close()
    var = (sst_bias_corrected_subset - 273.15)
    var = numpy.ma.masked_where(sst_quality_levels_subset.data < sst4investigator.sst_quality_level, var)
    return var

for idx, filename in enumerate(filenames):
    print(idx)
    print(filename)
    sst_sandwich[idx,:,:] = sst_extract(sst4investigator, filename)


mean_sst = numpy.nanmean(sst_sandwich, axis=0)

# open a new netCDF file for writing.
ncfile = netCDF4.Dataset('/tmp/spaceship/tmp_sst_merged.nc','w')
# create the x and y dimensions.
ncfile.createDimension('lat',numpy.shape(mean_sst)[0])
ncfile.createDimension('lon',numpy.shape(mean_sst)[1])
latitude = ncfile.createVariable('lat',numpy.dtype('float32').char,('lat',))
longitude = ncfile.createVariable('lon', numpy.dtype('float32').char, ('lon',))
data = ncfile.createVariable('sea_surface_temperature',numpy.dtype('float32').char,('lat','lon'))
# write data to variable.
data[:] = mean_sst
latitude[:] = lat_subset
longitude[:] = lon_subset
# close the file.
ncfile.close()

sst4investigator.sst_file_suffix = 'sst_merged.nc'
sst4investigator.sst_title_prefix = str('AVHRR SST, 8Day Ave, from: ' + start_date + ' to ' + end_date)
sst4investigator.make_sst_plot(is_it_a_tmp_file = True)

name_png = str('/tmp/spaceship/' + start_date + 'to' + end_date + '_8DayNightSkinQL3SST.png')
os.rename('/tmp/spaceship/tmp_sst_merged.png',name_png)
sst4investigator.copy_to_dir('/home/rjohnson/imstore/')
sst4investigator.email_the_png()
sst4investigator.cleanup()
