#!/usr/bin/env python
"""
This is a package to download sst or chlorophyll nc data, plot a subregion,
and email it to a list of emails addresses.

It is designed for sending chl and sst plots to research ships like the
RV Investigator during research expeditions.

"""
import numpy
import netCDF4
import datetime
import os
import glob
from urllib.request import urlretrieve
from thredds_crawler.crawl import Crawl
import sys
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap, cm
import shutil
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from email.mime.image import MIMEImage

class Sat2Ship():

	def __init__(self):
		"""
		Creates an instance of the Sat2Ship class and assigns the default attributes

		The default SST is a GHRSST AVHRR L3S 1 day night time only sst product produced by imos.
		The default CHL is a VIIRS 1day CI based Chlorophyll product from NASA.
		"""
		self.verbose = True
		self.chl_root_url = 'https://oceandata.sci.gsfc.nasa.gov/cgi/getfile/'
		self.sst_tds_url = 'http://thredds.aodn.org.au/thredds/catalog/IMOS/SRS/SST/ghrsst/L3S-1d/ngt/2018/catalog.html'
		self.maintainer_email = 'robtheoceanographer@gmail.com'
		self.customer_email = None
		self.northernmost_lat = None
		self.southernmost_lat = None
		self.easternmost_lon = None
		self.westernmost_lon = None
		self.tmp_working_dir = '/tmp/spaceship/'
		self.archive_dir = None
		self.sst_file_suffix = 'AVHRR_D-1d_night.nc'
		self.chl_file_suffix = '.L3m_DAY_SNPP_CHL_chl_ocx_4km.nc'
		self.chl_file_prefix = 'V'
		self.quality_level = 4
		self.plot_max = None
		self.sst_quality_level = 3
		self.sst_title_prefix = 'AVHRR SST, Night Skin Temp, QL > 3, '
		self.email_subject = "DAILY SAT IMAGE"
		self.email_body = "THIS IS AN AUTOMATED EMAIL AND THIS EMAIL ADDRESS IS NOT MONITORED - DO NOT REPLY.  WRITE TO robtheoceanographer@gmail.com IF YOU WANT TO CONTACT A HUMAN"
		self.fromaddr = "spaceship.sender@gmail.com"

	def copy_to_dir(self, a_dir):
		"""
		moves the images to a dir you tell me.
		"""
		file_list = glob.glob(str(self.tmp_working_dir + '/*.png'))
		assert os.path.exists(a_dir), "--- The destination directory does not exist."
		if len(file_list) > 0:
			shutil.copy(file_list[0], a_dir)
		return

	def cleanup(self):
		"""
			Deletes the tmp_working_dir and all of its contents.
		"""
		file_list = glob.glob(str(self.tmp_working_dir + '/*'))
		if len(file_list) > 0:
			for a_file in file_list:
				print('deleting: ' + a_file)
				os.remove(a_file)
			os.rmdir(self.tmp_working_dir)
		return

	def get_a_sat_file(self, the_date, the_product, period = 'DAILY', model = 'JOHNSON'):
		"""Attempts to download an nc data file for the data passed in.

		N.B. This assumes that the place you're downloading from is a thredds server and that the url you have defined
		in the sst_tds_url or chl_tds_url is a valid thredds catalog.

		INPUT:
			a_date = a string of the utc date something like 20180906 (e.g.the 7th of Sept 2018)
			the_product = either 'CHL' or 'SST'
			period : str
				either 'DAY' or '8DAY'
			model : str
				either 'OCX' or 'JOHNSON'

		RETURNS:
			exit status of 0 if all good.

		"""

		if self.verbose: print('Welcome to: get_a_sat_file')
		assert isinstance(the_date, str), 'The date you passed in is not a string.'
		assert the_product in ['CHL', 'SST'], 'The product you asked for was not either "CHL" or "SST".'
		assert period in ['DAILY', '8DAY'], 'The period you asked for was not either "DAILY" or "8DAY".'
		assert model in ['JOHNSON', 'OCX'], 'The model you asked for was not either "JOHNSON" or "OCX".'
		assert len(self.sst_tds_url) > 3, 'The sst thredds url you gave me seems to be too short.'
		if self.verbose: print(the_date, period, the_product, model)
		if not os.path.exists(self.tmp_working_dir):
			os.makedirs(self.tmp_working_dir)
		found = False
		if the_product == 'SST':
			if self.verbose: print('SST url: ', self.sst_tds_url)
			if self.verbose: print('Crawling the catalog for the file urls... this might take a while...')
			c = Crawl(self.sst_tds_url)
			print('Finished Crawling.')
			if self.verbose: print('An example of the crawler results: ', c.datasets[0])
			if self.verbose: print('Extracting the urls from the returned crawler object')
			urls = [s.get("url") for d in c.datasets for s in d.services if s.get("service").lower() == 'httpserver']
			if self.verbose: print('An example of the extracted urls: ', urls[0])
			for url in urls:
				if the_date in url:
					print('+++ Found a file for the date you passed in.')
					if self.verbose: print('+++ Downloading: ' + url)
					urlretrieve(url, str(self.tmp_working_dir + '/' + os.path.split(url)[1]))
					assert os.path.exists(str(self.tmp_working_dir + '/' + os.path.split(url)[1])), "--- File failed to download."
					found = True
		elif the_product == 'CHL':
			url_year = the_date[0:4]
			url_doy = str(datetime.datetime.strptime(the_date,'%Y%m%d').timetuple().tm_yday)
			if model == 'OCX':
				url_filename = str(self.chl_file_prefix + url_year + url_doy + self.chl_file_suffix)
				url_chl = str(self.chl_root_url + '/' + url_filename)
				print('Found a file for the date you passed in.')
				try:
					if self.verbose: print('+++ Downloading: ' + url_chl)
					urlretrieve(url_chl, str(os.path.join(self.tmp_working_dir, os.path.split(url_chl)[1])))
					assert os.path.exists(str(os.path.join(self.tmp_working_dir, os.path.split(url_chl)[1]))), "--- File failed to download."
					found = True
				except:
					print("--- Didn't find a file matching your date!")
					found = False
			elif  model == 'JOHNSON':
				# https://oceandata.sci.gsfc.nasa.gov/cgi/getfile/A2018266.L3m_DAY_RRS_Rrs_443_4km.nc
				rrs_443_filename = str('A' + url_year + url_doy + '.L3m_DAY_RRS_Rrs_443_4km.nc')
				rrs_490_filename = str('A' + url_year + url_doy + '.L3m_DAY_RRS_Rrs_488_4km.nc')
				rrs_555_filename = str('A' + url_year + url_doy + '.L3m_DAY_RRS_Rrs_555_4km.nc')
				url_443 = str(self.chl_root_url + '/' + rrs_443_filename)
				url_490 = str(self.chl_root_url + '/' + rrs_490_filename)
				url_555 = str(self.chl_root_url + '/' + rrs_555_filename)
				try:
					if self.verbose: print('+++ Downloading...' )
					urlretrieve(url_443, str(os.path.join(self.tmp_working_dir, os.path.split(url_443)[1])))
					assert os.path.exists(str(os.path.join(self.tmp_working_dir, os.path.split(url_443)[1]))), "--- File RRS 443 failed to download."
					urlretrieve(url_490, str(os.path.join(self.tmp_working_dir, os.path.split(url_490)[1])))
					assert os.path.exists(str(os.path.join(self.tmp_working_dir, os.path.split(url_490)[1]))), "--- File RRS 490 failed to download."
					urlretrieve(url_555, str(os.path.join(self.tmp_working_dir, os.path.split(url_555)[1])))
					assert os.path.exists(str(os.path.join(self.tmp_working_dir, os.path.split(url_555)[1]))), "--- File RRS 555 failed to download."
					found = True
				except:
					print("--- Didn't find a file matching your date!")
					found = False
				# generate a the johnson chl product
				rrs_443_file = str(os.path.join(self.tmp_working_dir, os.path.split(url_443)[1]))
				rrs_490_file = str(os.path.join(self.tmp_working_dir, os.path.split(url_490)[1]))
				rrs_555_file = str(os.path.join(self.tmp_working_dir, os.path.split(url_555)[1]))
				self.make_johnson_chl_file(rrs_443_file, rrs_490_file, rrs_555_file)
				# set the suffix to be whatever it is on the output nc file i write out of that function.
		else:
			print('--- THE PRODUCT YOU ASKED FOR IS NEITHER CHL NOR SST.')
			return
		assert found, "--- UNABLE TO FIND A FILE FOR THE DATE THAT YOU ENTERED."
		return



	def make_sst_plot(self, is_it_a_tmp_file = False):
		"""Looks in the tmp_working_dir and makes a plot of the SST data it finds there.
		Returns
		-------
		png file
			Saves a figure out into the tmp_working_dir
		"""
		assert self.northernmost_lat is not None, "northernmost_lat is not set."
		assert self.southernmost_lat is not None, "southernnmost_lat is not set."
		assert self.easternmost_lon is not None, "easternmost_lon is not set."
		assert self.westernmost_lon is not None, "westernmost_lon is not set."

		# check that there is an sst nc file in the tmp dir.
		filename = glob.glob(str(self.tmp_working_dir + '/*' + self.sst_file_suffix))
		assert len(filename) == 1, '--- Found more than one sst file in the working directory - please clean up then run the code again.'
		nc = netCDF4.Dataset(filename[0])
		nc.set_auto_maskandscale(True)
		
		if is_it_a_tmp_file:
			lon_subset = nc['lon'][:]
			lat_subset = nc['lat'][:]
			var = nc['sea_surface_temperature'][:]
		else:
			lon_subset = nc['lon'][:][numpy.abs(nc['lon'][:] - float(self.westernmost_lon)).argmin():numpy.abs(nc['lon'][:] - float(self.easternmost_lon)).argmin()]
			lat_subset = nc['lat'][:][numpy.abs(nc['lat'][:] - float(self.northernmost_lat)).argmin():numpy.abs(nc['lat'][:] - float(self.southernmost_lat)).argmin()]
			sst_bias_corrected = nc['sea_surface_temperature'][0,:,:] - nc['sses_bias'][0,:,:]
			sst_bias_corrected_subset = sst_bias_corrected[numpy.abs(nc['lat'][:] - float(self.northernmost_lat)).argmin():numpy.abs(nc['lat'][:] - float(self.southernmost_lat)).argmin(), numpy.abs(nc['lon'][:] - float(self.westernmost_lon)).argmin():numpy.abs(nc['lon'][:] - float(self.easternmost_lon)).argmin()]
			sst_quality_levels_subset = nc['quality_level'][0,:,:][numpy.abs(nc['lat'][:] - float(self.northernmost_lat)).argmin():numpy.abs(nc['lat'][:] - float(self.southernmost_lat)).argmin(), numpy.abs(nc['lon'][:] - float(self.westernmost_lon)).argmin():numpy.abs(nc['lon'][:] - float(self.easternmost_lon)).argmin()]
			var = (sst_bias_corrected_subset - 273.15)
			var = numpy.ma.masked_where(sst_quality_levels_subset.data < self.sst_quality_level, var)
		nc.close()
		
		#-- create figure and axes instances
		dpi = 200
		fig = plt.figure(figsize=(1100/dpi, 1100/dpi), dpi=dpi)
		cm_font = {'fontname':'cmtt10'}

		#-- create map
		# map = Basemap(projection='cyl',llcrnrlat=float(self.southernmost_lat),urcrnrlat= float(self.northernmost_lat),\
		#               resolution='h',  llcrnrlon=float(self.westernmost_lon),urcrnrlon=float(self.easternmost_lon))
		m = Basemap(projection='mill',llcrnrlat=float(self.southernmost_lat),urcrnrlat= float(self.northernmost_lat),\
					resolution='h',  llcrnrlon=float(self.westernmost_lon),urcrnrlon=float(self.easternmost_lon))

		# map = Basemap(projection='stere',lon_0=(float(self.easternmost_lon) - float(self.westernmost_lon)) / 2.0 + float(self.westernmost_lon),lat_0=((float(self.southernmost_lat) - float(self.northernmost_lat))/ 2.0) + float(self.northernmost_lat),\
		#         llcrnrlat=float(self.southernmost_lat),urcrnrlat= float(self.northernmost_lat),\
		#         llcrnrlon=float(self.westernmost_lon),urcrnrlon=float(self.easternmost_lon),\
		#         rsphere=6371200.,resolution='l',area_thresh=10000)

		# map = Basemap(projection='nsper',lon_0=(float(self.easternmost_lon) - float(self.westernmost_lon)) / 2.0 + float(self.westernmost_lon),lat_0=((float(self.southernmost_lat) - float(self.northernmost_lat))/ 2.0) + float(self.northernmost_lat),\
		#     satellite_height=(3000.*1000.),resolution='l')

		#-- draw coastlines, state and country boundaries, edge of map
		m.drawcoastlines()
		m.drawcountries()
		m.fillcontinents(color='#000000',lake_color='#99ffff')

		#-- create and draw meridians and parallels grid lines
		m.drawparallels(numpy.arange( -90., 90.,5.), labels=[1,0,0,0], fontsize=10, **cm_font)
		m.drawmeridians(numpy.arange(-180.,180.,5.), labels=[0,0,0,1], fontsize=10, **cm_font)

		#-- convert latitude/longitude values to plot x/y values
		x, y = m(*numpy.meshgrid(lon_subset, lat_subset))

		#-- contour levels
		if self.plot_max is None:
			if is_it_a_tmp_file:
				clevs = numpy.arange(int(numpy.nanmin(var)-1),int(numpy.nanmax(var)+1),1)
			else:
				clevs = numpy.arange(int(numpy.ma.min(var)-1),int(numpy.ma.max(var)+1),1)
		else:
			clevs = numpy.arange(int(numpy.ma.min(var)-1), int(self.plot_max),0.5)

		#-- draw filled contours
		cnplot = m.contourf(x,y,var,clevs,cmap='jet')

		#-- add colorbar
		cbar = m.colorbar(cnplot,location='bottom',pad="10%")      #-- pad: distance between map and colorbar
		cbar.set_label('deg C', **cm_font)
		ticklabs = cbar.ax.get_yticklabels()
		cbar.ax.set_yticklabels(ticklabs, **cm_font)
		#-- add plot title
		sst_title = str(self.sst_title_prefix + os.path.basename(filename[0])[0:8])
		if is_it_a_tmp_file:
			plt.title(str(self.sst_title_prefix),**cm_font)
		else:
			plt.title(sst_title,**cm_font)
		#-- set axis limits
		# plt.axis([float(self.westernmost_lon), float(self.easternmost_lon), float(self.southernmost_lat), float(self.northernmost_lat)])

		#-- display on screen
		# plt.show()
		plt.savefig(str(self.tmp_working_dir + '/' + os.path.basename(filename[0])[0:-3] + '.png'))


	def make_chl_plot(self, title_text = None):
		"""Looks in the tmp_working_dir and makes a plot of the chl data it finds there.

		Returns
		-------
		png file
			Saves a figure out into the tmp_working_dir
		"""
		assert self.northernmost_lat is not None, "northernmost_lat is not set."
		assert self.southernmost_lat is not None, "southernnmost_lat is not set."
		assert self.easternmost_lon is not None, "easternmost_lon is not set."
		assert self.westernmost_lon is not None, "westernmost_lon is not set."

		# check that there is an sst nc file in the tmp dir.
		filename = glob.glob(str(self.tmp_working_dir + '/*' + self.chl_file_suffix))
		assert len(filename) == 1, 'Found more than one chl file in the working directory - please clean up then run the code again.'
		nc = netCDF4.Dataset(filename[0])
		nc.set_auto_maskandscale(True)

		lon_subset = nc['lon'][:][numpy.abs(nc['lon'][:] - float(self.westernmost_lon)).argmin():numpy.abs(nc['lon'][:] - float(self.easternmost_lon)).argmin()]
		lat_subset = nc['lat'][:][numpy.abs(nc['lat'][:] - float(self.northernmost_lat)).argmin():numpy.abs(nc['lat'][:] - float(self.southernmost_lat)).argmin()]

		chl = nc['chl_ocx'][:,:]
		chl_subset = chl[numpy.abs(nc['lat'][:] - float(self.northernmost_lat)).argmin():numpy.abs(nc['lat'][:] - float(self.southernmost_lat)).argmin(), numpy.abs(nc['lon'][:] - float(self.westernmost_lon)).argmin():numpy.abs(nc['lon'][:] - float(self.easternmost_lon)).argmin()]

		nc.close()

		var = chl_subset
		#-- create figure and axes instances
		dpi = 200
		fig = plt.figure(figsize=(1100/dpi, 1100/dpi), dpi=dpi)
		cm_font = {'fontname':'cmtt10'}

		#-- create map
		# map = Basemap(projection='cyl',llcrnrlat=float(self.southernmost_lat),urcrnrlat= float(self.northernmost_lat),\
		#               resolution='h',  llcrnrlon=float(self.westernmost_lon),urcrnrlon=float(self.easternmost_lon))
		m = Basemap(projection='mill',llcrnrlat=float(self.southernmost_lat),urcrnrlat= float(self.northernmost_lat),\
					resolution='h',  llcrnrlon=float(self.westernmost_lon),urcrnrlon=float(self.easternmost_lon))

		# map = Basemap(projection='stere',lon_0=(float(self.easternmost_lon) - float(self.westernmost_lon)) / 2.0 + float(self.westernmost_lon),lat_0=((float(self.southernmost_lat) - float(self.northernmost_lat))/ 2.0) + float(self.northernmost_lat),\
		#         llcrnrlat=float(self.southernmost_lat),urcrnrlat= float(self.northernmost_lat),\
		#         llcrnrlon=float(self.westernmost_lon),urcrnrlon=float(self.easternmost_lon),\
		#         rsphere=6371200.,resolution='l',area_thresh=10000)

		# map = Basemap(projection='nsper',lon_0=(float(self.easternmost_lon) - float(self.westernmost_lon)) / 2.0 + float(self.westernmost_lon),lat_0=((float(self.southernmost_lat) - float(self.northernmost_lat))/ 2.0) + float(self.northernmost_lat),\
		#     satellite_height=(3000.*1000.),resolution='l')

		#-- draw coastlines, state and country boundaries, edge of map
		m.drawcoastlines()
		m.drawcountries()
		m.fillcontinents(color='#000000',lake_color='#99ffff')

		#-- create and draw meridians and parallels grid lines
		m.drawparallels(numpy.arange( -90., 90.,5.), labels=[1,0,0,0], fontsize=10, **cm_font)
		m.drawmeridians(numpy.arange(-180.,180.,5.), labels=[0,0,0,1], fontsize=10, **cm_font)

		#-- convert latitude/longitude values to plot x/y values
		x, y = m(*numpy.meshgrid(lon_subset,lat_subset))

		#-- contour levels
		# clevs = numpy.arange(int(0),int(math.ceil(numpy.ma.median(var))+1),0.01)
		clevs = numpy.arange(int(0), int(self.plot_max),0.01)

		#-- draw filled contours
		cnplot = m.contourf(x,y,var,clevs,cmap='jet')

		#-- add colorbar
		cbar = m.colorbar(cnplot,location='bottom',pad="10%")      #-- pad: distance between map and colorbar
		cbar.set_label('CHL mg m^-3', **cm_font)
		ticklabs = cbar.ax.get_yticklabels()
		cbar.ax.set_yticklabels(ticklabs, **cm_font)
		#-- add plot title
		if title_text == None:
			plt.title(str('VIIRS CHL, '+ os.path.basename(filename[0])[1:8]),**cm_font)
		else:
			plt.title(str(title_text),**cm_font)

		#-- set axis limits
		# plt.axis([float(self.westernmost_lon), float(self.easternmost_lon), float(self.southernmost_lat), float(self.northernmost_lat)])

		#-- display on screen
		# plt.show()
		plt.savefig(str(self.tmp_working_dir + '/' + os.path.basename(filename[0])[0:-3] + '.png'))


	def email_the_png(self):
		"""Looks in the tmp_working_dir for a png and then uses a pre-made gmail account to email it to the customer_email.

		"""
		file_list = glob.glob(str(self.tmp_working_dir + '/*.png'))
		assert len(file_list) > 0, "--- There are no pngs in the tmp_working_dir."
		filename = file_list[0]
		attachment = open(filename, "rb")
		msg = MIMEMultipart()
		msg['From'] = self.fromaddr
		msg['To'] = self.customer_email
		msg['Cc'] = self.maintainer_email
		msg['Subject'] = self.email_subject
		msg.attach(MIMEText(self.email_body, 'plain'))
		img = MIMEImage(attachment.read())
		img.add_header('Content-Disposition', "attachment; filename= %s" % str(os.path.basename(filename)))
		attachment.close()
		msg.attach(img)
		#part = MIMEBase('application', 'octet-stream')
		#part.set_payload((attachment).read())
		#encoders.encode_base64(part)
		#part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
        #msg.attach(part)
		server = smtplib.SMTP('smtp.gmail.com', 587)
		server.starttls()
		server.login(self.fromaddr, '^Mtp!shn3Nrq`kWu\#(?3g"XJM3ZGb[5x)_6HK%v')
		text = msg.as_string()
		server.sendmail(self.fromaddr, [self.customer_email, self.maintainer_email], text)
		server.quit()


	def make_johnson_chl_file(self, rrs_443_file, rrs_490_file, rrs_555_file):
		assert os.path.exists(rrs_443_file), "--- The RRS 443 file does not exist"
		assert os.path.exists(rrs_490_file), "--- The RRS 490 file does not exist"
		assert os.path.exists(rrs_555_file), "--- The RRS 555 file does not exist"
		rrs443, lats, lons = modis_parse_Rrs(rrs_443_file)
		rrs490, lats, lons = modis_parse_Rrs(rrs_490_file)
		rrs555, lats, lons = modis_parse_Rrs(rrs_555_file)
		so_chl = calculate_southern_ocean_chlorophyll(rrs443, rrs490, rrs555)
		assert numpy.nanmax(so_chl) > 0.0, 'The max of the calculated chl is not greater than zero.'
		# open a new netCDF file for writing.
		ncfile = netCDF4.Dataset('/tmp/spaceship/tmp_southern_ocean_chl.nc','w')
		# create the x and y dimensions.
		ncfile.createDimension('lat',numpy.shape(so_chl)[0])
		ncfile.createDimension('lon',numpy.shape(so_chl)[1])
		latitude = ncfile.createVariable('lat',numpy.dtype('float32').char,('lat',))
		longitude = ncfile.createVariable('lon', numpy.dtype('float32').char, ('lon',))
		data = ncfile.createVariable('chl_ocx',numpy.dtype('float32').char,('lat','lon'))
		# write data to variable.
		data[:] = so_chl
		latitude[:] = lats
		longitude[:] = lons
		# close the file.
		ncfile.close()
		print('+++ SUCCESS writing tmp chl file!')



# Calculate the Southern Ocean Chlorophyll.
def calculate_southern_ocean_chlorophyll(data_443, data_490, data_555):
	"""Calculate the modis aqua southern ocean Chl based on Johnson et al 2013.

	Args:
		data_443 (array) : an array that is scaled and had the fill values converted to nan.
		data_490 (array) : an array that is scaled and had the fill values converted to nan.
		data_555 (array) : an array that is scaled and had the fill values converted to nan.
	"""
	# Calculate the Rrr maximum band ratio log10(Rrs(443/555) > Rrs(490/555))
	MBR = numpy.log10(numpy.maximum((data_443/data_555),(data_490/data_555)))
	# Calculate SO Chl.
	SO_Chl = 10**(0.6994 - 2.0384 * MBR - 0.4656 * (MBR**2) + 0.4337 * (MBR**3))
	return SO_Chl



def modis_parse_Rrs(an_rrs_file):
	"""Pull out the Rrs data grid.
	Apply the scaling information on each Rrs data.

	Args:
		filepath (str) : Full path to the input file
	"""
	print('Processing %s' % an_rrs_file)
	# check file exists?
	nc = netCDF4.Dataset(an_rrs_file, 'r')
	key = os.path.basename(an_rrs_file)[21:28]
	variable = nc.variables[key]
	lats = nc.variables['lat'][:]
	lons = nc.variables['lon'][:]
	print('Variable = %s' % key)
	variable.set_auto_maskandscale(False)
	print('Setting the netCDF4 auto_maskandscale to False.')
	# Get the attributes
	attributes = [str(a) for a in variable.ncattrs()]
	# Get the fill value
	if '_FillValue' in attributes:
		fill_value = variable._FillValue
	else:
		fill_value = False
	# Get the scale factors, or set them to defaults (which would do nothing)
	scale_factor = 1.0 if 'scale_factor' not in attributes else variable.scale_factor
	add_offset = 0.0 if 'add_offset' not in attributes else variable.add_offset
	# Ensure that the scale factor is not 0, otherwise we would blow away the data
	if scale_factor == 0.0:
		scale_factor = 1.0
	# Get the data
	data = variable[:]
	# Mask out the data, if there is a fill value
	print('Masking fill values')
	if fill_value is not False:
		fill_mask = data == fill_value
	# Apply the scaling information
	print('Applying scaling: (data * %s) + %s' % (scale_factor, add_offset))
	data = (data * scale_factor) + add_offset
	# Remask the data, if there is a fill value
	print('Re-applying fill values')
	if fill_value is not False:
		data[fill_mask] = fill_value
	print('Turning fill values into nans')
	data[numpy.where(data == fill_value)] = numpy.nan
	return data, lats, lons
