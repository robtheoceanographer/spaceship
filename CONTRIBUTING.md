# Help us!

I'm really glad you're reading this, because we need your help!!

![alt text](./aux_files/helpme.jpeg)

## Issue tracker

The issue tracker is where bugs or issues with the project can be lodged/recorded for future investigation. It is probably a good idea to send someone an email at the same time so we know there is an issue.

When submitting an issue, please write out as much detail as you can. Here are some tips:
- Expected behavior: Describe what you thought would happen.
- Observed behavior: What actually happened.
- Steps to reproduce: Explain how someone reproduces the issue.
