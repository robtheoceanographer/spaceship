#!/usr/bin/env python
"""

an example of how to use the spaceship package.

"""
# Prototype the southern ocean chl plotting stuff.
import matplotlib
matplotlib.use('Agg')
import spaceship
import os
import arrow
import netCDF4
import numpy
import glob
import os
import datetime

chl4investigator = spaceship.Sat2Ship()
# chl4investigator.customer_email = 'nic.pittman@investigator.csiro.au'
chl4investigator.customer_email = 'robert.johnson@utas.edu.au'
chl4investigator.northernmost_lat = '-6.0'
chl4investigator.southernmost_lat = '-22.0'
chl4investigator.easternmost_lon = '122.0'
chl4investigator.westernmost_lon = '102.0'


arw = arrow.get(datetime.datetime(2018, 11, 10))

start_date = arw.format('YYYYMMDD')
# arw = arrow.utcnow()
# start_date = arw.shift(days=-10).format('YYYYMMDD')
print(start_date)
end_date = arw.shift(days=+9).format('YYYYMMDD')
print(end_date)


chl4investigator.cleanup()

chl4investigator.get_a_sat_file(start_date, 'CHL', 'DAILY', 'OCX')
chl4investigator.get_a_sat_file(arw.shift(days=+1).format('YYYYMMDD'), 'CHL', 'DAILY', 'OCX')
chl4investigator.get_a_sat_file(arw.shift(days=+2).format('YYYYMMDD'), 'CHL', 'DAILY', 'OCX')
chl4investigator.get_a_sat_file(arw.shift(days=+3).format('YYYYMMDD'), 'CHL', 'DAILY', 'OCX')
chl4investigator.get_a_sat_file(arw.shift(days=+4).format('YYYYMMDD'), 'CHL', 'DAILY', 'OCX')
chl4investigator.get_a_sat_file(arw.shift(days=+5).format('YYYYMMDD'), 'CHL', 'DAILY', 'OCX')
chl4investigator.get_a_sat_file(arw.shift(days=+6).format('YYYYMMDD'), 'CHL', 'DAILY', 'OCX')
chl4investigator.get_a_sat_file(arw.shift(days=+7).format('YYYYMMDD'), 'CHL', 'DAILY', 'OCX')
chl4investigator.get_a_sat_file(arw.shift(days=+8).format('YYYYMMDD'), 'CHL', 'DAILY', 'OCX')
chl4investigator.get_a_sat_file(arw.shift(days=+9).format('YYYYMMDD'), 'CHL', 'DAILY', 'OCX')




# merge the chl files.
chl4investigator.chl_file_suffix = '.L3m_DAY_SNPP_CHL_chl_ocx_4km.nc'
filenames = glob.glob(str(chl4investigator.tmp_working_dir + '/*' + chl4investigator.chl_file_suffix))

# must have lat and lon too...
nc = netCDF4.Dataset(filenames[0])
nc.set_auto_maskandscale(True)
lon_subset = nc['lon'][:][numpy.abs(nc['lon'][:] - float(chl4investigator.westernmost_lon)).argmin():numpy.abs(nc['lon'][:] - float(chl4investigator.easternmost_lon)).argmin()]
lat_subset = nc['lat'][:][numpy.abs(nc['lat'][:] - float(chl4investigator.northernmost_lat)).argmin():numpy.abs(nc['lat'][:] - float(chl4investigator.southernmost_lat)).argmin()]
nc.close()

sandwich = numpy.empty([20,lat_subset.shape[0], lon_subset.shape[0]])

def chl_extract(chl4investigator, filename):
    nc = netCDF4.Dataset(filename)
    nc.set_auto_maskandscale(True)
    attributes = [str(a) for a in nc['chl_ocx'].ncattrs()]
    # Get the fill value
    if '_FillValue' in attributes:
        fill_value = nc['chl_ocx']._FillValue
    else:
        fill_value = False
    lon_subset = nc['lon'][:][numpy.abs(nc['lon'][:] - float(chl4investigator.westernmost_lon)).argmin():numpy.abs(nc['lon'][:] - float(chl4investigator.easternmost_lon)).argmin()]
    lat_subset = nc['lat'][:][numpy.abs(nc['lat'][:] - float(chl4investigator.northernmost_lat)).argmin():numpy.abs(nc['lat'][:] - float(chl4investigator.southernmost_lat)).argmin()]
    print('Masking fill values')
    if fill_value is not False:
        fill_mask = nc['chl_ocx'][:,:].data == fill_value
    chl_ocx = nc['chl_ocx'][:,:]
    print('Re-applying fill values')
    if fill_value is not False:
        chl_ocx [fill_mask] = fill_value
    print('Turning fill values into nans')
    chl_ocx[numpy.where(chl_ocx  == fill_value)] = numpy.nan
    chl_ocx_subset = chl_ocx[numpy.abs(nc['lat'][:] - float(chl4investigator.northernmost_lat)).argmin():numpy.abs(nc['lat'][:] - float(chl4investigator.southernmost_lat)).argmin(), numpy.abs(nc['lon'][:] - float(chl4investigator.westernmost_lon)).argmin():numpy.abs(nc['lon'][:] - float(chl4investigator.easternmost_lon)).argmin()]

    nc.close()
    var = chl_ocx_subset
    return var


for idx, filename in enumerate(filenames):
    print(idx)
    print(filename)
    sandwich[idx,:,:] = chl_extract(chl4investigator, filename)


# mean_chl = numpy.nanmean(sandwich, axis=0)
mean_chl = numpy.nanmean(sandwich, axis=0)


# open a new netCDF file for writing.
ncfile = netCDF4.Dataset('/tmp/spaceship/tmp_chl_merged.nc','w')
# create the x and y dimensions.
ncfile.createDimension('lat',numpy.shape(mean_chl)[0])
ncfile.createDimension('lon',numpy.shape(mean_chl)[1])
latitude = ncfile.createVariable('lat',numpy.dtype('float32').char,('lat',))
longitude = ncfile.createVariable('lon', numpy.dtype('float32').char, ('lon',))
data = ncfile.createVariable('chl_ocx',numpy.dtype('float32').char,('lat','lon'))
# write data to variable.
data[:] = mean_chl
latitude[:] = lat_subset
longitude[:] = lon_subset
# close the file.
ncfile.close()


chl4investigator.chl_file_suffix = 'chl_merged.nc'

chl4investigator.plot_max = 1
chl4investigator.make_chl_plot(str('VIIRS CHL, 10Day Ave, from: ' + start_date + ' to ' + end_date))
name_png = str('/tmp/spaceship/' + start_date + 'to' + end_date + '_10DayVIIRSChl.png')
os.rename('/tmp/spaceship/tmp_chl_merged.png', name_png)
chl4investigator.copy_to_dir('/home/rjohnson/2.CURRENT_PROJECTS/spaceship')
# chl4investigator.email_the_png()
chl4investigator.cleanup()
