# SPACESHIP

Get space data and send it to a ship...

<p align="center">
  <img src="./aux_files/spaceship_logo.png" width="200px" height="200px"/></p>



This is a project designed to fetch ocean remote sensing data, make a plot of it, and send it to a research ship.

The default SST is a GHRSST AVHRR L3S 1 day night time only sst product produced by imos.
The default CHL is a VIIRS 1day CI based Chlorophyll product from NASA.

## Motivation
This project exists because i kept getting asked to send plots of satellite data to ships. I originally did it all manually but that sucks so now it's automated and put into this package.

## Code Example
```
import spaceship
# SST - 1 Day.
sst4investigator = spaceship.Sat2Ship()
sst4investigator.customer_email = 'robert.johnson@utas.edu.au'
sst4investigator.northernmost_lat = '-40.0'
sst4investigator.southernmost_lat = '-70.0'
sst4investigator.easternmost_lon = '170.0'
sst4investigator.westernmost_lon = '140.0'
date_of_interest = '20181003'
sst4investigator.get_a_sat_file(date_of_interest, 'SST')
sst4investigator.plot_max = None
sst4investigator.make_sst_plot()
sst4investigator.copy_to_dir('/home/rjohnson/imstore/')
sst4investigator.email_the_png()
sst4investigator.cleanup()

```

## Installation
At the moment you just run everything in the one directory and the spaceship code will import and work ok. eventually i will have this set up to be a proper python package that manages it's dependancies etc.

The list of python requirments is in `requirements.txt`


## Tests
To run the tests run the test.py script.

## Contribute
I am trying to use the semantic commit method: [https://seesparkbox.com/foundry/semantic_commit_messages](https://seesparkbox.com/foundry/semantic_commit_messages)

## License
This project is licensed under the MIT License:

Copyright (c) 2018 Robert Johnson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
